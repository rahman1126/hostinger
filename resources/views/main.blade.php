<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hostinger</title>
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/hostinger.css">
</head>
<body>
    <div id="app">
        <header-component></header-component>
        <hero-component></hero-component>
        <price-component></price-component>
        <service-component></service-component>
        <module-component></module-component>
        <cta-component></cta-component>
        <footer-component></footer-component>
    </div>
    <script src="{{ mix('js/app.js') }}"></script>
    <script src="https://kit.fontawesome.com/ee328cae56.js" crossorigin="anonymous"></script>
</body>
</html>